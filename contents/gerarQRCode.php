
<?php
//inicio QRCode
    include('../phpqrcode/qrlib.php');

    function limparString($str)
    {
        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        // $str = str_replace(" ","-",$str);
        return $str;
        // return strtolower($str);
    }
    
    $nome = 'Mário César Lanes Júnior'; 
    $nome = limparString($nome);
    $empresa = 'Clube da Web';
    $cargo = 'Front-End';
    $cel = '(13) 98836-9556';
    $fonetrabalho = '(13) 99711-7848';  
    $fonecasa = '(13) 2312-5678'; //Opcional
    $email = 'desenvolvimento@clubedaweb.com.br';
    $url = 'https://clubedaweb.com.br';
    $urlinked = 'https://www.linkedin.com/in/mariocesar121782139/'; //Opcional
    $token = '2131321';

    $cartao = 'BEGIN:VCARD'."\n"; 
    $cartao .= 'N: '.$nome."\n"; 
    $cartao .= 'ORG;Empresa: '.$empresa."\n";
    $cartao .= 'TITLE: '.$cargo."\n";
    $cartao .= 'TEL;Trabalho;VOICE: '.$fonetrabalho."\n"; 
    $cartao .= 'TEL;Residencial;VOICE: '.$fonecasa."\n";
    $cartao .= 'TEL;Celular;VOICE: '.$cel."\n"; 
    $cartao .= 'EMAIL;E-mail Empresarial: '.$email."\n";  
    $cartao .= 'URL;Site StartUP: '.$url."\n";
    $cartao .= 'URL;LinkedIn: '.$urlinked."\n";
    $cartao .= 'END:VCARD'."\n";
    $cartao .= ';CODIGO: '.$token."\n"; 
    QRcode::png($cartao, '../assets/qrcode/'.$token.'.png', QR_ECLEVEL_L, 6);

    echo '
        <body style="background-color:#444">
            <div style="text-align:center;padding-top:30px">
                <h1>QRCode</h1>
                <h2>'.$nome.'</h2>
                <img style="padding-top:20px;" src="../assets/qrcode/'.$token.'.png" />
            </div>
        </body>
    ';
//Fim QRCode

class Dispositivo
{

    private $user_agents;
    private $modelo;
    private $mobile;

    public function __construct()
    {
        $this->user_agents = array("iPhone","iPad","Android","webOS","BlackBerry","iPod","Symbian","IsGeneric");
        $this->mobile = FALSE;
    }

    public function getUserAgents()
    {
        return $this->user_agents;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function getDispositivo()
    {
        foreach($this->getUserAgents() as $user_agent){
            if (strpos($_SERVER['HTTP_USER_AGENT'], $user_agent) !== FALSE) {
                $this->mobile = TRUE;
                $this->modelo = $user_agent;
                break;
            }
        }
        if ($this->getMobile()){
            return "Acesso feito via ".strtolower($this->getModelo())."<br>";
    
        }else{
            return "Acesso feito via computador<br>";
        }
    }

}
$dispositivo = new Dispositivo();
echo $dispositivo->getDispositivo();

class IP
{
    private $client;
    private $forward;
    private $remote;
    private $ip;

    public function __construct()
    {
        $this->client = @$_SERVER['HTTP_CLIENT_IP'];
        $this->forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $this->remote  = $_SERVER['REMOTE_ADDR'];
    }

    public function getIp()
    {
        if (filter_var($this->getClient(), FILTER_VALIDATE_IP)) {
            $this->ip = $this->getClient();
        }
        elseif(filter_var($this->getForward(), FILTER_VALIDATE_IP))
        {
            $this->ip = $this->getForward();
        }
        else
        {
            $this->ip = $this->getRemote();
        }
        return $this->ip;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getForward()
    {
        return $this->forward;
    }

    public function getRemote()
    {
        return $this->remote;
    }

}

$ip = new IP();
echo $ip->getIp();

//Pega nome da máquina
    echo "<br>Nome da máquina: ".getenv("USERNAME");
?>
